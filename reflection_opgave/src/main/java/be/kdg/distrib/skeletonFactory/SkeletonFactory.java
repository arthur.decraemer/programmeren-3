package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.stubFactory.StubInvocationHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SkeletonFactory
{
    public static Object createSkeleton(Object o){
        InvocationHandler handler = new SkeletonInvocationHandler(o);
        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{Skeleton.class}, handler);
    }
}
