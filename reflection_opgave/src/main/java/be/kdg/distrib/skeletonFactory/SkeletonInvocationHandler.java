package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import com.sun.org.apache.xerces.internal.impl.XMLEntityScanner;
import org.omg.CORBA.OBJ_ADAPTER;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

public class SkeletonInvocationHandler implements InvocationHandler
{
    private final Object implementation;
    private final MessageManager messageManager;
    public SkeletonInvocationHandler(Object implementation)
    {
        this.implementation=implementation;
        this.messageManager= new MessageManager();
    }

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is aswered.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);
        if(methodName.equals("getAddress")){
            return messageManager.getMyAddress();
        }
        else if(methodName.equals("handleRequest")){
            //First argument should be method call message
            handleRequest((MethodCallMessage) args[0]);
        }
        else if(methodName.equals("run")){
            run();
        }
        return null;
    }

    private void run(){
        Thread thread=new Thread(){
            @Override
            public void run()
            {
                while (true)
                {
                    MethodCallMessage request = messageManager.wReceive();
                    try
                    {
                        handleRequest(request);
                    } catch (Throwable throwable)
                    {
                        throwable.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    private void handleRequest(MethodCallMessage methodCallMessage) throws Throwable{
        Map<String,String> parameters =methodCallMessage.getParameters();
        Method methodToBeExecuted=null;
            if(parameters.size()==0){
                //get method without params
                methodToBeExecuted=implementation.getClass().getDeclaredMethod(methodCallMessage.getMethodName());
            }
            else{
                //Get all methods
                Method[] methods=implementation.getClass().getDeclaredMethods();
                for(Method method:methods){
                    //Check method by name first
                    if(method.getName().equals(methodCallMessage.getMethodName())){
                        //if a given parameter for method could not be found in list of params set bool to true
                        boolean wrongParam=false;
                            for(Parameter parameter:method.getParameters()){
                                //check if simple or complex
                                if(isSimple(parameter.getType())){
                                    //Check key of map by parameter name
                                    if(parameters.get(parameter.getName())==null) wrongParam=true;
                                }
                            //When method contains all necessary
                            if(!wrongParam) methodToBeExecuted=method;
                        }
                    }
                }
            }
            Object[] argumentsToPass=convertParametersToCorrectType(methodCallMessage,methodToBeExecuted);
            Object object=methodToBeExecuted.invoke(implementation,argumentsToPass);

            if(methodToBeExecuted.getReturnType()==Void.class||methodToBeExecuted.getReturnType()==void.class){
                sendEmptyReply(methodCallMessage);
            }
            else if(isSimple(methodToBeExecuted.getReturnType())){
                MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
                reply.setParameter("result", object.toString());
                messageManager.send(reply, methodCallMessage.getOriginator());
            }
            else{
                //If object with fields is return
                MethodCallMessage reply=new MethodCallMessage(messageManager.getMyAddress(),"result");
                //Go through fields.
                for(Field arg:object.getClass().getDeclaredFields()){
                    arg.setAccessible(true);
                    //use argument number for assigning parameter
                    reply.setParameter("result"+"."+arg.getName(),arg.get(object).toString());

                }
                messageManager.send(reply,methodCallMessage.getOriginator());
            }
    }

    private Object[] convertParametersToCorrectType(MethodCallMessage message,Method toExecute)throws Throwable{
        //array holding converted values
        List<Object> parameterValues=new ArrayList<>();
        int i=0;
        for(Parameter parameter:toExecute.getParameters()){
            if(isSimple(parameter.getType())){
                //If type is simple
                parameterValues.add(returnResultAsType(message.getParameter(parameter.getName()),parameter.getType()));
            }
            else{
                //type is object
                //get subarguments to construct param
                Constructor constructor= parameter.getType().getConstructor();
                Object paramObj=constructor.newInstance();
                for(Field field:paramObj.getClass().getDeclaredFields()){
                    field.setAccessible(true);
                    Object fieldValue=returnResultAsType(message.getParameter("arg"+i+"."+field.getName()),field.getType());
                    field.set(paramObj,fieldValue);
                }
                parameterValues.add(paramObj);
            }
            i++;
        }

        return parameterValues.toArray();
    }

    public Object returnResultAsType(String result, Type typeToReturn){
        if(typeToReturn==String.class) return result;
        else if(typeToReturn==int.class||typeToReturn==Integer.class) return Integer.parseInt(result);
        else if(typeToReturn==char.class||typeToReturn==Character.class) return result.charAt(0);
        else if(typeToReturn==Boolean.class||typeToReturn==boolean.class)return Boolean.parseBoolean(result);
        else if(typeToReturn==Double.class||typeToReturn==double.class) return Double.parseDouble(result);
        else if(typeToReturn==Float.class||typeToReturn==float.class) return Float.parseFloat(result);
        else if(typeToReturn==Short.class||typeToReturn==short.class) return Short.parseShort(result);
        else if(typeToReturn==Long.class||typeToReturn==long.class) return Long.parseLong(result);
        else if(typeToReturn==Byte.class||typeToReturn==byte.class) return Byte.parseByte(result);

        else return null;
    }
    public boolean isSimple(Class c){
        return c.isPrimitive()
                || c == String.class
                || c == Double.class
                || c == Integer.class
                || c == Boolean.class
                || c == Character.class
                || c == Float.class
                || c == Short.class
                || c == Long.class
                || c == Byte.class;
    }
}
