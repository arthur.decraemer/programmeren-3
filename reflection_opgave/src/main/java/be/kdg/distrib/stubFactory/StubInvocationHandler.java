package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import sun.nio.ch.Net;

import javax.xml.ws.handler.MessageContext;
import java.lang.reflect.*;
import java.util.Map;

public class StubInvocationHandler implements InvocationHandler {
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public StubInvocationHandler(String ip, int port)
    {
        this.serverAddress = new NetworkAddress(ip,port);
        messageManager=new MessageManager();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        System.out.println("\tmethodName = " + methodName);
        MethodCallMessage message=new MethodCallMessage(messageManager.getMyAddress(),methodName);
        //setup message with given arguments
        message=setupMessage(message,args);
        //Send message and return response value
        return sendMessage(message,method);
    }

    public MethodCallMessage setupMessage(MethodCallMessage message,Object[] args) throws Throwable
    {
        if (args != null) {
            //keep track of argument number.
            int i=0;
            for (Object arg : args) {
                //Check if value can be read from arg directly or if it should go through fields for object;
                if(!directlyConvertToString(arg.getClass())){
                    //Go through fields.
                    for(Field arg2:arg.getClass().getDeclaredFields()){
                        arg2.setAccessible(true);
                        System.out.println("\targ = " + arg2.toString());
                        //use argument number for assigning parameter
                        message.setParameter("arg"+i+"."+arg2.getName(),arg2.get(args[i]).toString());
                    }
                    //next parameter will have i+1
                    i++;
                }
                else{
                    //Directly read.
                    System.out.println("\targ = " + arg);
                    //use argument number for assigning parameter
                    message.setParameter("arg"+i,arg.toString());
                    //next parameter will have i+1
                    i++;
                }
            }
        }
        return message;
    }

    public Object sendMessage(MethodCallMessage message,Method method) throws Throwable
    {
        messageManager.send(message,serverAddress);
        //Map all the return arguments.
        Map<String,String> returnParams=messageManager.wReceive().getParameters();
        if(returnParams.size()==0){
            return null;
        }
        else if(returnParams.size()==1){
            return returnResultAsType(returnParams.get("result"),method.getReturnType());
        }
        else{
            return returnMultipleFieldsResult(method,returnParams);
        }
    }
    public Object returnMultipleFieldsResult(Method method, Map<String, String> returnParams) throws Throwable{
        //Get constructor of object return type
        Constructor constructor=method.getReturnType().getConstructor();
        //Create object of return type with default constructor
        Object toReturn =constructor.newInstance();
        for(String paramName:returnParams.keySet()){
            //Get field name
            Field field=toReturn.getClass().getDeclaredField(paramName.split("\\.")[1]);
            //set accessibility of field
            field.setAccessible(true);
            //Set field value to correct return value
            field.set(toReturn,returnResultAsType(returnParams.get(paramName),field.getType()));
        }
        return toReturn;
    }
    public Object returnResultAsType(String result, Type typeToReturn){
        if(typeToReturn==String.class) return result;
        else if(typeToReturn==int.class||typeToReturn==Integer.class) return Integer.parseInt(result);
        else if(typeToReturn==char.class||typeToReturn==Character.class) return result.charAt(0);
        else if(typeToReturn==Boolean.class||typeToReturn==boolean.class)return Boolean.parseBoolean(result);
        else if(typeToReturn==Double.class||typeToReturn==double.class) return Double.parseDouble(result);
        else if(typeToReturn==Float.class||typeToReturn==float.class) return Float.parseFloat(result);
        else if(typeToReturn==Short.class||typeToReturn==short.class) return Short.parseShort(result);
        else if(typeToReturn==Long.class||typeToReturn==long.class) return Long.parseLong(result);
        else if(typeToReturn==Byte.class||typeToReturn==byte.class) return Byte.parseByte(result);

        else return null;
    }

    public boolean directlyConvertToString(Class c){
        return c.isPrimitive()
                || c == String.class
                || c == Double.class
                || c == Integer.class
                || c == Boolean.class
                || c == Character.class
                || c == Float.class
                || c == Short.class
                || c == Long.class
                || c == Byte.class;
    }
}
