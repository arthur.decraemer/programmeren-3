package server;

import client.Document;
import client.DocumentImpl;
import client.DocumentSkeleton;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public final class ServerStub implements Server
{
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;
    private final NetworkAddress docSkeletonAddress;

    public ServerStub(NetworkAddress serverAddress,NetworkAddress docSkeletonAddress)
    {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
        this.docSkeletonAddress=docSkeletonAddress;
    }

    /**
     * Waits for a reply and checks if it contains no return-value.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    @Override
    public void log(Document document)
    {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "log");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    @Override
    public Document create(String s)
    {
        return new DocumentImpl(s);
    }

    @Override
    public void toUpper(Document document)
    {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toUpper");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        MethodCallMessage response=messageManager.wReceive();
        if(!response.getMethodName().equals("result")) System.out.println("The server did not produce the expected response.");
        String responseText=response.getParameter("text");
        document.setText(responseText);
    }

    @Override
    public void toLower(Document document)
    {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toLower");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        MethodCallMessage response=messageManager.wReceive();
        if(!response.getMethodName().equals("result")) System.out.println("The server did not produce the expected response.");
        String responseText=response.getParameter("text");
        document.setText(responseText);
    }

    @Override
    public void type(Document document, String text)
    {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "type");
        message.setParameter("text", text);
        message.setParameter("skeletonIp",docSkeletonAddress.getIpAddress());
        message.setParameter("skeletonPort",String.valueOf(docSkeletonAddress.getPortNumber()));
        messageManager.send(message,serverAddress);

        checkEmptyReply();
        System.out.println("Server finished typing");
    }
}
