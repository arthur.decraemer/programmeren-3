package client;

import communication.MessageManager;
import communication.MethodCallMessage;

public class DocumentSkeleton
{
    public MessageManager getMessageManager()
    {
        return messageManager;
    }

    private final MessageManager messageManager;
    private final Document document;

    public DocumentSkeleton(Document document)
    {
        this.messageManager=new MessageManager();
        System.out.println("my address = " + this.messageManager.getMyAddress());
        this.document = document;
    }

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is aswered.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }
    /**
     * Handles an incomming request.
     *
     * @param request the request that is being handled.
     */
    private void handleRequest(MethodCallMessage request)
    {
        String methodName = request.getMethodName();
        if("append".equals(methodName)){
            handleAppend(request);
        }else
        {
            System.out.println("ServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void handleAppend(MethodCallMessage request)
    {
        document.append(request.getParameter("text").charAt(0));
        sendEmptyReply(request);
    }

    /**
     * The main loop for this skeleton.
     */
    public void run()
    {
        while (true)
        {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }


}
