package client;

import communication.MessageManager;
import communication.NetworkAddress;
import server.ServerStub;

public class StartNonDistributed {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.err.println("Usage: java Client <IP> <Port>");
            System.exit(1);
        }
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAdress = new NetworkAddress(args[0], port);
        DocumentImpl document = new DocumentImpl();
        DocumentSkeleton documentSkeleton= new DocumentSkeleton(document);
        ServerStub serverStub = new ServerStub(serverAdress,documentSkeleton.getMessageManager().getMyAddress());

        Client client = new Client(serverStub,document);
        Thread thread=new Thread(){
            @Override
            public void run()
            {
                client.run();
            }
        };
        thread.start();
        Thread thread2=new Thread(){
            @Override
            public void run()
            {
                documentSkeleton.run();
            }
        };
        thread2.start();
    }
}
