package server;

import client.Document;
import client.DocumentImpl;
import client.DocumentStub;
import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public final class ServerSkeleton
{
    private final MessageManager messageManager;
    private final Server server;

    public ServerSkeleton()
    {
        this.messageManager = new MessageManager();
        System.out.println("my address = " + messageManager.getMyAddress());
        this.server = new ServerImpl();
    }

    /**
     * Sends reply with no return-value to the originator of a request.
     *
     * @param request the request that is aswered.
     */
    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleLog(MethodCallMessage request)
    {
        Document receivedDocument = new DocumentImpl(request.getParameter("text"));
        server.log(receivedDocument);
        sendEmptyReply(request);
    }

    private void handleToUpper(MethodCallMessage request)
    {
        Document receivedDocument = new DocumentImpl(request.getParameter("text"));
        server.toUpper(receivedDocument);
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        response.setParameter("text",receivedDocument.getText());
        messageManager.send(response,request.getOriginator());
    }
    private void handleToLower(MethodCallMessage request)
    {
        Document receivedDocument = new DocumentImpl(request.getParameter("text"));
        server.toLower(receivedDocument);
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        response.setParameter("text",receivedDocument.getText());
        messageManager.send(response,request.getOriginator());
    }

    private void handleType(MethodCallMessage request)
    {
        NetworkAddress returnAdress = new NetworkAddress(request.getParameter("skeletonIp"),Integer.parseInt(request.getParameter("skeletonPort")));
        Document document = new DocumentStub(returnAdress);
        String text = request.getParameter("text");
        server.type(document,text);
        sendEmptyReply(request);
    }

    /**
     * Handles an incomming request.
     *
     * @param request the request that is being handled.
     */
    private void handleRequest(MethodCallMessage request)
    {
        //System.out.println("ContactsSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("log".equals(methodName))
        {
            handleLog(request);
        }
        else if("toUpper".equals(methodName)){
            handleToUpper(request);
        }
        else if("toLower".equals(methodName)){
            handleToLower(request);
        }
        else if("type".equals(methodName)){
            handleType(request);
        }else
        {
            System.out.println("ServerSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    /**
     * The main loop for this skeleton.
     */
    public void run()
    {
        while (true)
        {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}
