package client;

import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

public class DocumentStub implements Document
{
    private final NetworkAddress serverAddress;
    private final MessageManager messageManager;

    public DocumentStub(NetworkAddress serverAddress)
    {
        this.serverAddress = serverAddress;
        this.messageManager = new MessageManager();
    }

    /**
     * Waits for a reply and checks if it contains no return-value.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    @Override
    public String getText()
    {
        return null;
    }

    @Override
    public void setText(String text)
    {

    }

    @Override
    public void append(char c)
    {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "append");
        message.setParameter("text", Character.toString(c));
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    @Override
    public void setChar(int position, char c)
    {

    }
}
